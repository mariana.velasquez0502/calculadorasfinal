import numpy as np
import sympy as sp
import math as mt
x = sp.symbols('x')

def f(func, val):
    return func.subs(x,val)

# Explicación de la función:
# 
# Parámetros:
# xi = vector de x experimentales
# yi = vector de y experimentales
# grado = grado de la función de regresión que se va a calcular
# 
# Retorno:
# funcion = La propia función de la regresión
# coef = el coeficiente de correlación
# Para ambos los valores son redondeados a 5 decimales


def ajuste(xi, yi, grado):

    n = len(xi)
    xi = np.array(xi)
    yi = np.array(yi)

    # print("xi: ",xi)
    # print("yi: ",yi)

    xajuste = []
    yajuste = []
    sol_ajuste = 0
    funcion = ""
    for i in range(0,grado+1):
        a =[]

        for j in range(0,grado+1):
            if i == 0 and j == 0:
                a.append(n)
            else:
                x1 = np.power(xi, i+j)
                a.append(np.sum(x1))
            
        xajuste.append(a)

    for i in range(0,grado+1):
        x1 = np.power(xi, i)
        y1 = np.multiply(x1,yi)
        yajuste.append(np.sum(y1))

    sol_ajuste = np.linalg.solve(xajuste, yajuste)

    for i in range(0,len(sol_ajuste)):
        if i == 0:
            funcion = str(round(sol_ajuste[i],5)) + "*x**" + str(i)
        else:
            funcion = funcion + "+" + str(round(sol_ajuste[i],5)) + "*x**" + str(i)

    funcion = sp.sympify(funcion)

    prom_y = sum(yi)/len(yi)

    st = 0
    sr = 0

    for i in range(0,n):
        st = st + (yi[i] - prom_y)**2
        sr = sr + (yi[i] - f(funcion, xi[i]))**2

    # print("Valor: ",(st-sr)/st)
    coef = mt.sqrt((st-sr)/st)

    return funcion, round(coef,5)


# Ejemplo de funcionamiento, descomentar para probar.
# sol = ajuste([15.,14.,4.,7.], [41.,42.,19.,17.], 1)

# print("Función de regresión")
# print(sol[0])
# print("Coeficiente de correlación")
# print(sol[1])

