import numpy

def gauss(matrix, vector, m, n):

    x = numpy.zeros((int(m)))

    for k in range(0,int(m)):
        for r in range(k+1,int(m)):
            factor=(matrix[r,k]/matrix[k,k])
            vector[r]=vector[r]-(factor*vector[k])
            for c in range(0,int(n)):
                matrix[r,c]=matrix[r,c]-(factor*matrix[k,c])

    #sustitución hacia atrás

    x[int(m)-1]=vector[int(m)-1]/matrix[int(m)-1,int(m)-1]

    for r in range(int(m)-2,-1,-1):
        suma=0
        for c in range(0,int(n)):
            suma=suma+matrix[r,c]*x[c]
        x[r]=(vector[r]-suma)/matrix[r,r]

    return x