import numpy as np
from numpy.polynomial import Polynomial as P
import array as arr


def calcpolyroots(data):

    coef = data.split('/')

    x=[float(i) for i in coef]

    coeff = P(x)
    roots = [str(x) for x in coeff.roots()]
    res=[]
    for s in roots:
        print(s[len(s)-1])
        if s[len(s)-2]=='j':
            s = s[:-2]
            s=s+"i)"
            res.append(s)
        elif len(s)<25:
            if(s[len(s)-1]==")"):
                s = s[:-5]
                s=s+")"
            elif(s[len(s)-1]=="j"):
                s = s[:-1]
                s=s+"i"
                
            res.append(s)
                # print(s)
                
            # print(s)
            
        else:
            res.append(s)
    return(res)