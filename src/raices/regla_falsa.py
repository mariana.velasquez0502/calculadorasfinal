#Librerías necesarias para la ejecución
import sympy as sp
from sympy import *

#Línea que marca que x va a ser tratado como un símbolo
x,y = sp.symbols('x y')

#Función para reemplazar x por un número y retornar el valor de la función
def f(a,ec):
    #La función subs reemplaza el símbolo x por el valor de a y lo retorna
    return ec.subs(x,a)

def reglaFalse(ec,xi,xs,tol):
    i = 0
    raiz=[]
    raiz.insert(0,0)
    error = 1
    hay = True
    xa = xs - (xs-xi)/(f(xs,ec)-f(xi,ec)) * f(xs,ec)

    while abs(error) > tol:
        i = i + 1
        if f(xi,ec) * f(xa,ec) < 0:
            xs = xa
        else:
            xi = xa
        xa = xs - (xs-xi)/(f(xs,ec)-f(xi,ec)) * f(xs,ec)
        raiz.append(xa)
        error=(raiz[i]-raiz[i-1])/raiz[i]
        if i == 50:
            hay = False
            break

    if hay:
        return i+1, float(xi), float(xs), float(f(xi,ec)), float(f(xs,ec)), float(xa), float(f(xa,ec)), abs(error)
    else:
        errorI = "máximas iteraciones"
        return errorI