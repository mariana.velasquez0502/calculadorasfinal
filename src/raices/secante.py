import sympy as sp
from math import *
import tkinter as tk
from tkinter import messagebox as msg
x,y = sp.symbols('x y')

def f(a,ec):
    return ec.subs(x,a)

def secante(ec,x0,x1,tol):
    raiz=[]
    raiz.insert(0,0)
    i = 0
    x2 = 0
    error = 1
    hay = True
    while abs(error) > tol:
        x2 = x1 - (f(x1,ec)*(x1-x0))/(f(x1,ec)-f(x0,ec))
        raiz.append(x2)
        x0 = x1
        x1 = x2
        i += 1
        error=(raiz[i]-raiz[i-1])/raiz[i]
        if i == 50:
            hay = False
            break

    if hay:
        return i+1, float(x1), float(x0), float(x2),abs(error)
    else:
        errorI = "máximas iteraciones"
        return errorI

        ##print('La raíz %r fue hallada en %f iteraciones'%(x2,i+1))
