import sympy as sp
from sympy import *
from numpy import *
import tkinter as tk
from tkinter import messagebox as msg
x,y = sp.symbols('x y')


def f(a,ec):
    return ec.subs(x,a)


def biseccion(ec,xi,xs,tol):
    signo = ""
    limite = ""
    i = 0
    raiz=[]
    raiz.append(xs)
    error = 1
    hay = True
    xa = 0.0
    if f(xi,ec)*f(xs,ec) < 0:
        xa = (xi + xs) / 2.0

        while abs(error) > tol:
            i = i + 1
            #i, float(xi), float(xs), float(f(xi,ec)), float(f(xs,ec)), float(xa), float(f(xa,ec)), abs(error)
            if f(xi,ec) * f(xa,ec) < 0:
                xs = xa
                signo = "negativo"
                limite = "superior"
            else:
                xi = xa
                signo = "positivo"
                limite = "inferior"
            xa = (xi + xs) / 2.0
            raiz.append(xa)
            error=(raiz[i-1]-raiz[i])/raiz[i]
            if i == 50:
                hay = False
                break
        if hay:
            return i+1, float(xi), float(xs), float(f(xi,ec)), float(f(xs,ec)), float(xa), float(f(xa,ec)), abs(error)
            #return float(xa)
        else:
            errorI = "máximas iteraciones"
            return errorI
    else:
        return "no esta"
