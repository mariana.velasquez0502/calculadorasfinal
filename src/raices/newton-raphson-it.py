import sympy as sp
from sympy import *
x,y = sp.symbols('x y')

print ("Método de Newton-Raphson")
ec = sp.sympify(input('Ingrese la función f(x) = '))
def f(a):
    return ec.subs(x,a)

def deri(eq,a):
    return Derivative(eq,x,evaluate=True).subs(x,a)

x0 = float(input('Introduce el valor de inicio '))
# tol = float(input('Introduce el error '))
raiz=[]
raiz.insert(0,0)
i = 0
error = 1
hay = True

print('{:^12}{:^12}{:^12}{:^12}'.format(
    'iteración', 'punto inicial', 'raíz','error'))

while i < 4:
    x1=x0-(f(x0)/deri(ec,x0))
    raiz.append(x1)
    print('{:^12}{:^12.6f}{:^12.6f}{:^12.12f}'.format(
        i+1, float(x0), float(x1),abs(error)))
    i += 1
    x0 = x1
    error=(raiz[i]-raiz[i-1])/raiz[i]
    # print (x0)
    # if i == 50:
    #     print("Número de iteraciones máximo alcanzado")
    #     hay = False
    #     break

if hay:
    print('{:^12}{:^12.6f}{:^12.6f}{:^12.12f}'.format(
        i+1, float(x1), float(x0),abs(error)))
    print('La raíz %r fue hallada en %f iteraciones con un error de '%(x1,i+1),abs(error))