from sympy import *
import sympy as sp
# We have to create a "symbol" called x
x = Symbol('x')

def derivada(funcion, punto):
    p = Derivative(funcion, x, evaluate=True)
    s = Derivative(p, x, evaluate=True)
    function = lambdify(x, funcion)
    dFunction = lambdify(x, p)
    ddFunction = lambdify(x, s)
    return function(punto), p, dFunction(punto), s, ddFunction(punto)