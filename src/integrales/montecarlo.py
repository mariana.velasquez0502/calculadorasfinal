import sympy as sp
from sympy.sets import Interval
import random
x = sp.Symbol('x')

def f(y, punto):
    return y.subs(x,punto)

def int_montecarlo(funcion, a, b, maximo, tot):
    n = 0
    for i in range(tot):
        xi = (b-a)*random.random()+a
        yi = random.random()*maximo
        if yi <= f(funcion, xi):
            n += 1
    return (n/tot)*(b-a)*maximo


# funcion = sp.sympify(input('función: '))
# a = float(sp.sympify(input('limite inferior: ')))
# b = float(sp.sympify(input('limite superior: ')))
# maximo = float(input('maximo: '))
# tot = int(input('puntos totales: '))

# print('integral: ', int_montecarlo(funcion,a,b,maximo,tot))
# print(f(funcion, a))