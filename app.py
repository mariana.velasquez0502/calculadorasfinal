from flask import Flask, render_template, request, redirect, flash
import src.integrales.integral_rectangulos as rec
import src.integrales.integral_trapecios as tra
import src.integrales.simpson13 as simpon
import src.integrales.simpson38 as simp
import src.integrales.montecarlo as mnt
import src.raices.bisecc as bisecci
import src.raices.newtonraphson as newtonr
import src.raices.secante as secant
import src.raices.regla_falsa as reglafalsa
import src.matrices.gauss as gauss
import src.Derivadas.derivative as deriva
import src.conversiones.conversion_bases as base
import ctypes as types
import src.conversiones.IEEE754 as ieee754
import src.raices.polynomialRoots as poly
import sympy as sp
import numpy as np
import src.matrices.operaciones as matrix
import src.matrices.min_cuad as curv

app = Flask(__name__,instance_relative_config=True)

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/integrales')
def integrales():
    return render_template('./pages/integrales.html')

@app.route('/graficadora')
def graficadora():
    return render_template('./pages/graficadora.html')

@app.route('/nosotros')
def nosotros():
    return render_template('./pages/nosotros.html')

@app.route('/conversiones')
def conversiones():
    return render_template('./pages/conversiones.html')

# AQUÍ ESTÁ EL DE RAÍCES DE POLINOMIOS
# @app.route("/raicespolinomios", methods=['GET','POST'])
# def raicespolinomios():   
#     return render_template('./raicespolinomios/raicespolinomios.html')

    
@app.route('/derivadas', methods=['GET','POST'])
def derivada():   
    if request.method == "POST":
        x = sp.symbols('x')
        resultado = 0.0
        funcion = request.form['funcion']
        punto = request.form['punto']
        if 'Borrar' in request.form:
            return render_template('./derivadas/derivadas.html')
        elif 'Calcular' in request.form:
            try:
                funcion = sp.sympify(request.form['funcion'])
                punto = float(sp.sympify(request.form['punto']))
            except:
                return render_template('./derivadas/derivadas-e.html',funcion=funcion, punto=punto, errorI='Hubo un error en el ingreso de datos, intente de nuevo')
            else:
                resultado = deriva.derivada(funcion,punto)
                return render_template('./derivadas/derivadas-h.html',funcion=funcion, punto=punto, resultado=resultado)
    return render_template('./derivadas/derivadas.html')
    

@app.route('/raices')
def raices():
    return render_template('./pages/raices.html')

@app.route('/matrices')
def matrices():
    return render_template('./pages/matrices.html')

@app.route('/Msuma', methods=['GET','POST'])
def suma():
    if request.method == "POST":
        trans = ''
        inputMatrizA = request.form['vectorA']
        numFilasA = request.form['FilA']
        numColumnasA = request.form['ColA']
        filasA = inputMatrizA.split('],[')
        columnasA = []
        matrizA = []
        inputMatrizB = request.form['vectorB']
        numFilasB = request.form['FilA']
        numColumnasB = request.form['ColA']
        filasB = inputMatrizB.split('],[')
        columnasB = []
        matrizB = []
        if "Calcular" in request.form:
            for i in range(0,len(filasA)):
                col = filasA[i].split(',')
                for j in range(0,len(col)):
                    col[j] = col[j].replace('[','')
                    col[j] = col[j].replace(']','')
                    try:
                        columnasA.append(float(sp.sympify(col[j])))
                    except:
                        return render_template('./matrices/suma/MsumaE.html', error="Uno o más valores ingresados en la matriz no son válidos en la matriz A, intente de nuevo")
                matrizA.append(columnasA)
                columnasA = []
            matrizA = np.array(matrizA)
            for i in range(0,len(filasB)):
                col = filasB[i].split(',')
                for j in range(0,len(col)):
                    col[j] = col[j].replace('[','')
                    col[j] = col[j].replace(']','')
                    try:
                        columnasB.append(float(sp.sympify(col[j])))
                    except:
                        return render_template('./matrices/suma/MsumaE.html', error="Uno o más valores ingresados en la matriz no son válidos en la matriz B, intente de nuevo")
                matrizB.append(columnasB)
                columnasB = []
            matrizB = np.array(matrizB)
            trans = matrix.suma(matrizA,matrizB)
            return render_template('./matrices/suma/MsumaR.html', matrizA=matrizA, matrizB=matrizB,trans=trans)
        elif "Borrar" in request.form:
            return render_template('./matrices/suma/Msuma.html')


    return render_template('./matrices/suma/Msuma.html')

@app.route('/Mresta', methods=['GET','POST'])
def resta():
    if request.method == "POST":
        trans = ''
        inputMatrizA = request.form['vectorA']
        numFilasA = request.form['FilA']
        numColumnasA = request.form['ColA']
        filasA = inputMatrizA.split('],[')
        columnasA = []
        matrizA = []
        inputMatrizB = request.form['vectorB']
        numFilasB = request.form['FilA']
        numColumnasB = request.form['ColA']
        filasB = inputMatrizB.split('],[')
        columnasB = []
        matrizB = []
        if "Calcular" in request.form:
            for i in range(0,len(filasA)):
                col = filasA[i].split(',')
                for j in range(0,len(col)):
                    col[j] = col[j].replace('[','')
                    col[j] = col[j].replace(']','')
                    try:
                        columnasA.append(float(sp.sympify(col[j])))
                    except:
                        return render_template('./matrices/resta/MrestaE.html', error="Uno o más valores ingresados en la matriz no son válidos en la matriz A, intente de nuevo")
                matrizA.append(columnasA)
                columnasA = []
            matrizA = np.array(matrizA)
            for i in range(0,len(filasB)):
                col = filasB[i].split(',')
                for j in range(0,len(col)):
                    col[j] = col[j].replace('[','')
                    col[j] = col[j].replace(']','')
                    try:
                        columnasB.append(float(sp.sympify(col[j])))
                    except:
                        return render_template('./matrices/resta/MrestaE.html', error="Uno o más valores ingresados en la matriz no son válidos en la matriz B, intente de nuevo")
                matrizB.append(columnasB)
                columnasB = []
            matrizB = np.array(matrizB)
            trans = matrix.resta(matrizA,matrizB)
            return render_template('./matrices/resta/MrestaR.html', A=matrizA,B=matrizB,trans=trans)
        elif "Borrar" in request.form:
            return render_template('./matrices/resta/Mresta.html')


    return render_template('./matrices/resta/Mresta.html')

@app.route('/detInvTras', methods=['GET','POST'])
def detInvTras():
    if request.method == "POST":
        trans = ''
        inv = ''
        det = 0.0
        inputMatriz = request.form['vectorA']
        numFilas = request.form['FilA']
        numColumnas = request.form['ColA']
        filas = inputMatriz.split('],[')
        columnas = []
        matriz = []
        if "Calcular" in request.form:
            for i in range(0,len(filas)):
                col = filas[i].split(',')
                for j in range(0,len(col)):
                    col[j] = col[j].replace('[','')
                    col[j] = col[j].replace(']','')
                    try:
                        columnas.append(float(sp.sympify(col[j])))
                    except:
                        return render_template('./matrices/detInvTras/detInvTrasE.html', error="Uno o más valores ingresados en la matriz no son válidos, intente de nuevo")
                matriz.append(columnas)
                columnas = []
            matriz = np.array(matriz)
            print(matriz)
            trans = matrix.transpuesta(matriz)
            if numFilas == numColumnas:
                det = matrix.determinante(matriz)
                # det = round(det,6)
                try:
                    inv = matrix.inversa(matriz)
                except :
                    return render_template('./matrices/detInvTras/detInvTrasR.html', matriz=matriz, trans=trans, inv="No es posible calcular", det=det)
            else:
                inv = det = "La matriz no es cuadrada, no se puede calcular"
            return render_template('./matrices/detInvTras/detInvTrasR.html',  matriz=matriz, trans=trans, det=det, inv=inv)
        elif "Borrar" in request.form:
            return render_template('./matrices/detInvTras/detInvTras.html')


    return render_template('./matrices/detInvTras/detInvTras.html')

@app.route('/matrizMultiplicacion', methods=['GET','POST'])
def matrizMultiplicacion():
    if request.method == "POST":
        trans = ''
        inputMatrizA = request.form['vectorA']
        numFilasA = request.form['FilA']
        numColumnasA = request.form['ColA']
        filasA = inputMatrizA.split('],[')
        columnasA = []
        matrizA = []
        inputMatrizB = request.form['vectorB']
        numFilasB = request.form['ColA']
        numColumnasB = request.form['ColB']
        filasB = inputMatrizB.split('],[')
        columnasB = []
        matrizB = []
        if "Calcular" in request.form:
            for i in range(0,len(filasA)):
                col = filasA[i].split(',')
                for j in range(0,len(col)):
                    col[j] = col[j].replace('[','')
                    col[j] = col[j].replace(']','')
                    try:
                        columnasA.append(float(sp.sympify(col[j])))
                    except:
                        return render_template('./matrices/Mmultiplicacion/multiplicacionE.html', error="Uno o más valores ingresados en la matriz no son válidos en la matriz A, intente de nuevo")
                matrizA.append(columnasA)
                columnasA = []
            matrizA = np.array(matrizA)
            for i in range(0,len(filasB)):
                col = filasB[i].split(',')
                for j in range(0,len(col)):
                    col[j] = col[j].replace('[','')
                    col[j] = col[j].replace(']','')
                    try:
                        columnasB.append(float(sp.sympify(col[j])))
                    except:
                        return render_template('./matrices/Mmultiplicacion/multiplicacionE.html', error="Uno o más valores ingresados en la matriz no son válidos en la matriz B, intente de nuevo")
                matrizB.append(columnasB)
                columnasB = []
            matrizB = np.array(matrizB)
            trans = matrix.prodPunto(matrizA,matrizB)
            return render_template('./matrices/Mmultiplicacion/multiplicacionR.html', A=matrizA,B=matrizB,trans=trans)
        elif "Borrar" in request.form:
            return render_template('./matrices/Mmultiplicacion/multiplicacion.html')


    return render_template('./matrices/Mmultiplicacion/multiplicacion.html')

    
@app.route('/gaussJordan', methods=['GET','POST'])
def gaussJordan():
    if request.method == "POST":
        trans = ''
        inputMatrizA = request.form['vectorA']
        numFilasA = request.form['FilA']
        numColumnasA = request.form['FilA']
        filasA = inputMatrizA.split('],[')
        columnasA = []
        matrizA = []
        inputMatrizB = request.form['vectorB']
        numFilasB = request.form['FilA']
        numColumnasB = request.form['ColB']
        filasB = inputMatrizB.split('],[')
        columnasB = []
        matrizB = []
        if "Calcular" in request.form:
            for i in range(0,len(filasA)):
                col = filasA[i].split(',')
                for j in range(0,len(col)):
                    col[j] = col[j].replace('[','')
                    col[j] = col[j].replace(']','')
                    try:
                        columnasA.append(float(sp.sympify(col[j])))
                    except:
                        return render_template('./matrices/gauss/gaussJordanE.html', error="Uno o más valores ingresados en la matriz A no son válidos, intente de nuevo")
                matrizA.append(columnasA)
                columnasA = []
            matrizA = np.array(matrizA)
            for i in range(0,len(filasB)):
                col = filasB[i].split(',')
                for j in range(0,len(col)):
                    col[j] = col[j].replace('[','')
                    col[j] = col[j].replace(']','')
                    try:
                        columnasB.append(float(sp.sympify(col[j])))
                    except:
                        return render_template('./matrices/gauss/gaussJordanE.html', error="Uno o más valores ingresados en el vector B no son válidos, intente de nuevo")
                matrizB.append(columnasB)
                columnasB = []
            matrizB = np.array(matrizB)
            try:
                trans = matrix.solve(matrizA,matrizB)
            except:
                return render_template('./matrices/gauss/gaussJordanE.html', error="No se puede resolver la matriz")
            trans = matrix.solve(matrizA,matrizB)
            print(trans)
            return render_template('./matrices/gauss/gaussJordanR.html', matriz=matrizA, vector=matrizB,trans=trans)
        elif "Borrar" in request.form:
            return render_template('./matrices/gauss/gaussJordan.html')

    return render_template('./matrices/gauss/gaussJordan.html')

@app.route('/ajusteCurvas', methods=['GET','POST'])
def ajusteCurvas():
    if request.method == "POST":
        grado1 = []
        grado2 = []
        grado3 = []
        grado4 = []
        grado5 = []
        grado6 = []
        inputVectores = request.form['vectorA']
        filas = inputVectores.split('],[')
        # print(type (filas[0]))
        columnas = []
        matriz = []
        if "Calcular" in request.form:
            for i in range(0,len(filas)):
                col = filas[i].split(',')
                for j in range(0,len(col)):
                    col[j] = col[j].replace('[','')
                    col[j] = col[j].replace(']','')
                    try:
                        columnas.append(float(sp.sympify(col[j])))
                    except:
                        return render_template('./ajusteCurvas/ajusteCurvasE.html', error="Uno o más valores ingresados en la matriz no son válidos, intente de nuevo")
                matriz.append(columnas)
                columnas = []
            grado1 = curv.ajuste(matriz[0],matriz[1],1)
            grado2 = curv.ajuste(matriz[0],matriz[1],2)
            try:
                grado3 = curv.ajuste(matriz[0],matriz[1],3)
            except:
                grado3 = ["No es posible calcular","No es posible calcular"]
                grado4 = ["No es posible calcular","No es posible calcular"]
                grado5 = ["No es posible calcular","No es posible calcular"]
                grado6 = ["No es posible calcular","No es posible calcular"]
            else: 
                try:
                    grado4 = curv.ajuste(matriz[0],matriz[1],4)
                except:
                    grado4 = ["No es posible calcular","No es posible calcular"]
                    grado5 = ["No es posible calcular","No es posible calcular"]
                    grado6 = ["No es posible calcular","No es posible calcular"]
                else:
                    try:
                        grado5 = curv.ajuste(matriz[0],matriz[1],5)
                    except:
                        grado5 = ["No es posible calcular","No es posible calcular"]
                        grado6 = ["No es posible calcular","No es posible calcular"]
                    else:
                        try:
                            grado6 = curv.ajuste(matriz[0],matriz[1],6)
                        except:
                            grado6 = ["No es posible calcular","No es posible calcular"]
                        else:
                            return render_template('./ajusteCurvas/ajusteCurvasR.html', grado1=grado1,grado2=grado2,grado3=grado3,grado4=grado4,grado5=grado5,grado6=grado6, xi=matriz[0], yi=matriz[1])
            return render_template('./ajusteCurvas/ajusteCurvasR.html', grado1=grado1,grado2=grado2,grado3=grado3,grado4=grado4,grado5=grado5,grado6=grado6, xi=matriz[0], yi=matriz[1])
    return render_template('./ajusteCurvas/ajusteCurvas.html')    

@app.route("/bases", methods=['GET','POST'])
def bases():   
    if request.method == "POST":
        decimal = str(request.form['decimal'])
        binario = str(request.form['binario'])
        octal = str(request.form['octal'])
        hexa = str(request.form['hexa'])

        if 'Calcular' in request.form:
            if 'decimal' in request.form and decimal != '' and binario == '' and octal == '' and hexa == '':
                if base.decimaltoBinary(decimal) != "No es un número decimal":
                    binario = base.decimaltoBinary(decimal)
                    octal = base.decimaltoOctal(decimal)
                    hexa = base.decimaltoHexa(decimal)
                    return render_template('./bases/bases.html',decimal=decimal, binario=binario, octal=octal, hexa=hexa, errorI='')
                else:
                    return render_template('./bases/bases.html',decimal=decimal, binario=binario, octal=octal, hexa=hexa, errorI='Error: El numero ingresado no es Decimal')
            elif 'binario' in request.form and binario != '' and decimal == '' and octal == '' and hexa == '':
                if base.convertBinToHex(binario) != "No es un número binario":
                    decimal = base.binaryToDecimal(binario,len(binario))
                    octal = base.convertBinToOct(binario)
                    hexa = base.convertBinToHex(binario)
                    return render_template('./bases/bases.html',decimal=decimal, binario=binario, octal=octal, hexa=hexa, errorI='')
                else:
                    return render_template('./bases/bases.html',decimal=decimal, binario=binario, octal=octal, hexa=hexa, errorI='Error: El numero ingresado no es Binario')
            elif 'octal' in request.form and octal != '' and decimal == '' and binario == '' and hexa == '':
                if base.octToBinary(octal) != "No es un número octal":
                    decimal = base.octToBinary(octal)
                    binario = base.binaryToDecimal(base.octToBinary(octal),len(base.octToBinary(octal)))
                    hexa = base.convertBinToHex(base.octToBinary(octal))
                    return render_template('./bases/bases.html',decimal=decimal, binario=binario, octal=octal, hexa=hexa, errorI='')
                else:
                    return render_template('./bases/bases.html',decimal=decimal, binario=binario, octal=octal, hexa=hexa, errorI='Error: El numero ingresado no es Octal')
            elif 'hexa' in request.form and hexa != '' and decimal == '' and binario == '' and octal == '':
                if base.hexToBinary(hexa) != "No es un número hexadecimal":
                    decimal = base.hexToBinary(hexa)
                    binario = base.convertBinToOct(base.hexToBinary(hexa))
                    octal = base.binaryToDecimal(base.hexToBinary(hexa),len(base.hexToBinary(hexa)))
                    return render_template('./bases/bases.html',decimal=decimal, binario=binario, octal=octal, hexa=hexa, errorI='')
                else:
                    return render_template('./bases/bases.html',decimal=decimal, binario=binario, octal=octal, hexa=hexa, errorI='Error: El numero ingresado no es Hexadecimal')
            else:
                return render_template('./bases/bases.html',decimal=decimal, binario=binario, octal=octal, hexa=hexa, errorI="Error: Ingrese solo la base a convertir")

        elif 'Borrar' in request.form:
            return render_template('./bases/bases.html')
    return render_template('./bases/bases.html')

@app.route("/biseccion", methods=['GET','POST'])
def bise():   
    if request.method == "POST":
        x,y = sp.symbols('x y')
        resultado = 0.0
        funcion = request.form['funcion']
        izq = request.form['izq']
        der = request.form['der']
        error = request.form['error']

        if 'Borrar' in request.form:
            return render_template('./Biseccion/Bisecc.html')

        elif 'Calcular' in request.form:
            try:
                funcion = sp.sympify(request.form['funcion'])
                izq = float(sp.sympify(request.form['izq']))
                der = float(sp.sympify(request.form['der']))
                error = float(request.form['error'])
            except:
                return render_template('./Biseccion/biseccionE.html',funcion=funcion, izq=izq, der=der, error=error, errorI='Hubo un error en el ingreso de datos, intente de nuevo')
            else:
                if izq >= der:
                    return render_template('./Biseccion/biseccionE.html',funcion=funcion, izq=izq, der=der, error=error, errorI='El valor del extremo izquierdo debe ser menor al del extremo derecho, intente de nuevo')
                else:
                    if bisecci.biseccion(funcion,izq,der,error) != "máximas iteraciones" and bisecci.biseccion(funcion,izq,der,error) != "no esta":
                        resultado = bisecci.biseccion(funcion,izq,der,error)
                        return render_template('./Biseccion/Biseccion.html',funcion=funcion, izq=izq, der=der, error=error, resultado=resultado)
                    elif bisecci.biseccion(funcion,izq,der,error) == "máximas iteraciones":
                        return render_template('./Biseccion/biseccionE.html',funcion=funcion, izq=izq, der=der, error=error, errorI='Número de iteraciones máximo alcanzado')
                    else:
                        return render_template('./Biseccion/biseccionE.html',funcion=funcion, izq=izq, der=der, error=error, errorI='Las imágenes de los puntos tienen el mismo signo, no se puede calcular la raíz por este método')
    return render_template('./Biseccion/Bisecc.html')

@app.route("/newtonraphson", methods=['GET','POST'])
def newton():   
    if request.method == "POST":
        x,y = sp.symbols('x y')
        resultado = 0.0
        funcion = request.form['funcion']
        punto = request.form['punto']
        error = request.form['error']
        if 'Borrar' in request.form:
            return render_template('./newtonraphson/newtonraphson.html')
        elif 'Calcular' in request.form:
            try:
                funcion = sp.sympify(request.form['funcion'])
                punto = float(sp.sympify(request.form['punto']))
                error = float(request.form['error'])
            except:
                return render_template('./newtonraphson/newtonE.html',funcion=funcion, punto=punto, error=error, errorI='Hubo un error en el ingreso de datos, intente de nuevo')
            else:
                if newtonr.calc_Newton(funcion,punto,error) != "máximas iteraciones":
                    resultado = newtonr.calc_Newton(funcion,punto,error)
                    return render_template('./newtonraphson/newtonraphson-result.html',funcion=funcion, punto=punto, error=error, resultado=resultado)
                else:
                    return render_template('./newtonraphson/newtonE.html',funcion=funcion, punto=punto, error=error, errorI='Número de iteraciones máximo alcanzado')
    return render_template('./newtonraphson/newtonraphson.html')

@app.route("/secante", methods=['GET','POST'])
def secante():   
    if request.method == "POST":
        x,y = sp.symbols('x y')
        resultado = 0.0
        funcion = request.form['funcion']
        izq = request.form['izq']
        der = request.form['der']
        error = request.form['error']
        if 'Borrar' in request.form:
            return render_template('./secante/secan.html')
        elif 'Calcular' in request.form:
            try:
                funcion = sp.sympify(request.form['funcion'])
                izq = float(sp.sympify(request.form['izq']))
                der = float(sp.sympify(request.form['der']))
                error = float(request.form['error'])
            except:
                return render_template('./secante/secanteE.html',funcion=funcion, izq=izq, der=der, error=error, errorI='Hubo un error en el ingreso de datos, intente de nuevo')
            else:
                if izq >= der:
                    return render_template('./secante/secanteE.html',funcion=funcion, izq=izq, der=der, error=error, errorI='El valor del extremo izquierdo debe ser menor al del extremo derecho, intente de nuevo')
                else:
                    if secant.secante(funcion,izq,der,error) != "máximas iteraciones":
                        resultado = secant.secante(funcion,izq,der,error)
                        return render_template('./secante/secante.html',funcion=funcion, izq=izq, der=der, error=error, resultado=resultado)
                    else:
                        return render_template('./secante/secanteE.html',funcion=funcion, izq=izq, der=der, error=error, errorI='Número de iteraciones máximo alcanzado')
    return render_template('./secante/secan.html')

@app.route("/reglafalsa", methods=['GET','POST'])
def reglaF():   
    if request.method == "POST":
        x,y = sp.symbols('x y')
        resultado = 0.0
        funcion = request.form['funcion']
        izq = request.form['izq']
        der = request.form['der']
        error = request.form['error']
        if 'Borrar' in request.form:
            return render_template('./reglafalsa/regla.html')
        elif 'Calcular' in request.form:
            try:
                funcion = sp.sympify(request.form['funcion'])
                izq = float(sp.sympify(request.form['izq']))
                der = float(sp.sympify(request.form['der']))
                error = float(request.form['error'])
            except:
                return render_template('./reglafalsa/reglaE.html',funcion=funcion, izq=izq, der=der, error=error, errorI='Hubo un error en el ingreso de datos, intente de nuevo')
            else:
                if izq >= der:
                    return render_template('./reglafalsa/reglaE.html',funcion=funcion, izq=izq, der=der, error=error, errorI='El valor del extremo izquierdo debe ser menor al del extremo derecho, intente de nuevo')
                else:
                    if reglafalsa.reglaFalse(funcion,izq,der,error) != "máximas iteraciones":
                        resultado = reglafalsa.reglaFalse(funcion,izq,der,error)
                        return render_template('./reglafalsa/reglafalsa.html',funcion=funcion, izq=izq, der=der, error=error, resultado=resultado)
                    else:
                       return render_template('./reglafalsa/reglaE.html',funcion=funcion, izq=izq, der=der, error=error, errorI='Número de iteraciones máximo alcanzado') 
    return render_template('./reglafalsa/regla.html')


@app.route("/rectangulos", methods=['GET','POST'])
def rect():   
    if request.method == "POST":
        x = sp.Symbol('x')
        resultado = 0.0
        funcion = request.form['funcion']
        izq = request.form['izq']
        der = request.form['der']
        particiones = request.form['particiones']

        if 'Borrar' in request.form:
            return render_template('./rectangulos/rectangulos.html')
        if 'Calcular' in request.form:
            try:
                funcion = sp.sympify(request.form['funcion'])
                izq = float(sp.sympify(request.form['izq']))
                der = float(sp.sympify(request.form['der']))
                particiones = int(request.form['particiones'])
            except:
                return render_template('./rectangulos/integralE.html',funcion=funcion, izq=izq, der=der, particiones=particiones, errorI="Hubo un error en el ingreso de datos, intente de nuevo")
            else:
                if izq >= der:
                    return render_template('./rectangulos/integralE.html',funcion=funcion, izq=izq, der=der, particiones=particiones, errorI="El valor del extremo izquierdo debe ser menor al del extremo derecho, intente de nuevo")
                else:
                    resultado = rec.int_rectangulos(funcion,izq,der,particiones)
                    return render_template('./rectangulos/integral-rectangulos.html',funcion=funcion, izq=izq, der=der, particiones=particiones, resultado=resultado)
    return render_template('./rectangulos/rectangulos.html')


@app.route("/trapecios", methods=['GET','POST'])
def trap():  
    if request.method == "POST":
        x = sp.Symbol('x')
        resultado = 0.0
        funcion = request.form['funcion']
        izq = request.form['izq']
        der = request.form['der']
        particiones = request.form['particiones']
        if 'Borrar' in request.form:
                return render_template('./trapecios/trapecios.html')
        elif 'Calcular' in request.form:
            try:
                funcion = sp.sympify(request.form['funcion'])
                izq = float(sp.sympify(request.form['izq']))
                der = float(sp.sympify(request.form['der']))
                particiones = int(request.form['particiones'])
            except:
                return render_template('./trapecios/trapeciosE.html',funcion=funcion, izq=izq, der=der, particiones=particiones, errorI="Hubo un error en el ingreso de datos, intente de nuevo")
            else:
                if izq >= der:
                    return render_template('./trapecios/trapeciosE.html',funcion=funcion, izq=izq, der=der, particiones=particiones, errorI="El valor del extremo izquierdo debe ser menor al del extremo derecho, intente de nuevo")
                else:
                    resultado = tra.calcular(funcion,izq,der,particiones)
                    return render_template('./trapecios/integral-trapecios.html',funcion=funcion, izq=izq, der=der, particiones=particiones, resultado=resultado)
    return render_template('./trapecios/trapecios.html')


@app.route('/simpson13', methods=['GET', 'POST'])
def simp13():
    if request.method == "POST":
        x = sp.Symbol('x')
        result = 0.0
        function = request.form['functionName']
        left = request.form['left']
        right = request.form['right']
        parts = request.form['numberOfPart']
        if 'Borrar' in request.form:
            return render_template('./simpson13/simpson13.html')
        elif 'Calcular' in request.form:
            try:
                function = sp.sympify(request.form['functionName'])
                left = float(request.form['left'])
                right = float(request.form['right'])
                parts = int(request.form['numberOfPart'])
            except:
                return render_template('./simpson13/simpson13E.html', functionName = function, left = left, right = right, numberOfPart = parts, errorI="Hubo un error en el ingreso de datos, intente de nuevo")
            else:
                if left >= right:
                    return render_template('./simpson13/simpson13E.html', functionName = function, left = left, right = right, numberOfPart = parts, errorI="El valor del extremo izquierdo debe ser menor al del extremo derecho, intente de nuevo")
                else:
                    result = simpon.calcular(function, left, right, parts)
                    return render_template('./simpson13/integral-simpson13.html', functionName = function, left = left, right = right, numberOfPart = parts, result = result)
    return render_template('./simpson13/simpson13.html')

@app.route('/simpson38', methods=['GET', 'POST'])
def simp38():
    if request.method == "POST":
        x = sp.Symbol('x')
        result = 0.0
        function = request.form['functionName']
        left = request.form['left']
        right = request.form['right']
        parts = request.form['numberOfPart']
        if 'Borrar' in request.form:
            return render_template('./simpson38/simpson38.html')
        elif 'Calcular' in request.form:
            try:
                function = sp.sympify(request.form['functionName'])
                left = float(request.form['left'])
                right = float(request.form['right'])
                parts = int(request.form['numberOfPart'])
            except:
                return render_template('./simpson38/simpson38E.html', functionName = function, left = left, right = right, numberOfPart = parts, errorI="Hubo un error en el ingreso de datos, intente de nuevo")
            else:
                if left >= right:
                    return render_template('./simpson38/simpson38E.html', functionName = function, left = left, right = right, numberOfPart = parts, errorI="El valor del extremo izquierdo debe ser menor al del extremo derecho, intente de nuevo")
                else:
                    result = simp.calcular(function, left, right, parts)
                    return render_template('./simpson38/integral-simpson38.html', functionName = function, left = left, right = right, numberOfPart = parts, result = result)
    return render_template('./simpson38/simpson38.html')

@app.route('/montecarlo', methods=['GET','POST'])
def montecarlo():
    if request.method == "POST":
        x = sp.Symbol('x')
        resultado = 0.0
        funcion = request.form['funcion']
        izq = request.form['izq']
        der = request.form['der']
        maximo = request.form['maximo']
        puntos = request.form['puntos']
        if 'Borrar' in request.form:
            return render_template('./montecarlo/montecarlo.html')
        elif 'Calcular' in request.form:
            try:
                funcion = sp.sympify(request.form['funcion'])
                izq = float(sp.sympify(request.form['izq']))
                der = float(sp.sympify(request.form['der']))
                maximo = float(sp.sympify(request.form['maximo']))
                puntos = int(request.form['puntos'])
            except:
                return render_template('./montecarlo/montecarloE.html',funcion=funcion, izq=izq, der=der, maximo=maximo, puntos=puntos, errorI="Hubo un error en el ingreso de datos, intente de nuevo")
            else:
                for i in str(puntos).lower():
                    if i == "-":
                        return render_template('./montecarlo/montecarloE.html',funcion=funcion, izq=izq, der=der, maximo=maximo, puntos=puntos, errorI="El punto no acepta numero negativo")
                    else:
                        resultado = mnt.int_montecarlo(funcion,izq,der,maximo,puntos)
                        return render_template('./montecarlo/integral-montecarlo.html',funcion=funcion, izq=izq, der=der, maximo=maximo, puntos=puntos, resultado=resultado)
    return render_template('./montecarlo/montecarlo.html')

@app.route('/raicespolinomios', methods=['GET', 'POST'])
def polyroots():
    if request.method == 'POST':
        # x = sp.Symbol('x y')
        resultado = 0
        #funcion = sp.sympify(request.form['funcion'])
        coeficientes = request.form['coeficientes']
        coef = coeficientes.split('/')

        for x in coef:
            try:
                float(x)
            except:
                return render_template('./polyroots/polyrootsE.html', coeficientes = coeficientes, resultado = resultado, errorI = 'Error al ingreso de datos')
        
        if 'Calcular' in request.form:
            resultado = poly.calcpolyroots(coeficientes)
            # print(resultado)
            return render_template('./polyroots/polyroots.html', coeficientes = coeficientes, resultado = resultado)
        elif 'Borrar' in request.form:
            return render_template('./polyroots/poly.html')
    return render_template('./polyroots/poly.html')

@app.route('/standardieee754', methods=['GET','POST'])
def standardieee754():
    if request.method == "POST":
        inserted=""
        if request.form['decimal']: 
            decimal=str(request.form['decimal'])
            inserted="decimal" 
        else: 
            decimal=str(0.0)

        if request.form['binario']: 
            binario=str(request.form['binario'])
            inserted="binario" 
        else: 
            binario=str(0.0)

        if request.form['ieee32']: 
            ieee32=str(request.form['ieee32'])
            inserted="ieee32" 
        else: 
            ieee32=str(0.0)
        
        if request.form['ieee64']: 
            ieee64=str(request.form['ieee64'])
            inserted="ieee64" 
        else: 
            ieee64=str(0.0)
        
        # binario=float(request.form['binario'])
        # ieee32=float(request.form['iee32'])
        # ieee64=float(request.form['ieee64'])
        if 'Calcular' in request.form:
            # resultado = mnt.int_montecarlo(funcion,izq,der,maximo,puntos)
            if inserted == "decimal" and binario=='0.0' and ieee32=='0.0' and ieee64=='0.0':
                if base.decimaltoBinary(decimal) != "No es un número decimal":
                    binario=ieee754.DecToBin(str(decimal))
                    ieee32=ieee754.iee32(str(binario))
                    ieee64=ieee754.iee64(str(binario))
                else:
                    return render_template('./IEEE/conv_ieee.html',decimal=decimal, binario=binario, ieee32=ieee32, ieee64=ieee64, errorI='Error: El numero ingresado no es Decimal')
            elif inserted=="binario" and decimal=='0.0' and ieee32=='0.0' and ieee64=='0.0':
                if base.convertBinToHex(binario) != "No es un número binario":
                    decimal=ieee754.binaryToDecimal(str(binario),len((str(binario))))
                    ieee32=ieee754.iee32(str(binario))
                    ieee64=ieee754.iee64(str(binario))
                else:
                    return render_template('./IEEE/conv_ieee.html',decimal=decimal, binario=binario, ieee32=ieee32, ieee64=ieee64, errorI='Error: El numero ingresado no es Binario')
            elif inserted=="ieee32" and binario=='0.0' and decimal=='0.0' and ieee64=='0.0':
                if base.convertBinToHex(ieee32) != "No es un número binario":
                    results = ieee754.fromStandard(1, ieee32)
                    decimal = results[0]
                    binario = results[1]
                    ieee64 = results[2]
                else:
                    return render_template('./IEEE/conv_ieee.html',decimal=decimal, binario=binario, ieee32=ieee32, ieee64=ieee64, errorI='Error: El numero ingresado no es Binario')
            elif inserted=="ieee64" and binario=='0.0' and decimal=='0.0' and ieee32=='0.0':
                if base.convertBinToHex(ieee64) != "No es un número binario":
                    results = ieee754.fromStandard(2, ieee64)
                    decimal = results[0]
                    binario = results[1]
                    ieee32 = results[2]
                else:
                    return render_template('./IEEE/conv_ieee.html',decimal=decimal, binario=binario, ieee32=ieee32, ieee64=ieee64, errorI='Error: El numero ingresado no es Binario')
            else:
                return render_template('./IEEE/conv_ieee.html',decimal=decimal, binario=binario, ieee32=ieee32, ieee64=ieee64, errorI='Error: Ingrese unicamente el campo a convertir')

            return render_template('./IEEE/conv_ieee.html',decimal=decimal, binario=binario, ieee32=ieee32, ieee64=ieee64)
        elif 'Borrar' in request.form:
            return render_template('./IEEE/ieee.html')
    return render_template('./IEEE/ieee.html')

# def suma(numero1, numero2):
#     return numero1+numero2

# def resta(numero1, numero2):
#     return numero1-numero2

if __name__ == '__main__':
    app.run(debug=True, port=4000)